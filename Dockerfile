FROM python:3.10-slim-bullseye

RUN pip install poetry

ADD ./src/poetry.lock ./src/pyproject.toml /src/


WORKDIR /src

ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN poetry config virtualenvs.create false
RUN poetry install

RUN useradd -ms /bin/bash noroot
RUN chown -R noroot:noroot /src
USER noroot

COPY src /src


EXPOSE 8000

CMD ["/src/entrypoint.sh"]