from pydantic import BaseSettings, PostgresDsn


class Settings(BaseSettings):
    PG_DSN: PostgresDsn
    SECRET: str
    PROJECT_NAME: str
    DEBUG: bool = True
    ALGORITHM: str
    SALT_SIZE: int


settings = Settings()
