from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.orm import relationship

from app.common.base_model import BaseModel


class PermissionModel(BaseModel):
    code = Column(String, unique=True, nullable=False)
    role_id = Column(ForeignKey('role.id'), nullable=False)
    role = relationship('RoleModel', back_populates='permissions')
