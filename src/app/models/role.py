from sqlalchemy import Column, String

from app.common.base_model import BaseModel


class RoleModel(BaseModel):
    name = Column(String(64), unique=True)
