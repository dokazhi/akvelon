from sqlalchemy import Column, String, Enum

from app.common.base_model import BaseModel


class UserModel(BaseModel):
    __tablename__ = 'users'
    username = Column(String(255), unique=True, nullable=False)
    password = Column(String(255))
    role = Column(Enum('admin', 'user', name='roles'), default='user')
