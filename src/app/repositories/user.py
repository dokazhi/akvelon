from typing import TypeVar, Type, Any

from sqlalchemy import select

from app.common.base_model import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session

ModelType = TypeVar('ModelType', bound=BaseModel)


class Repository:
    def __init__(self, session: AsyncSession | Session, model: Type[ModelType]):
        self.session = session
        self.model = model

    async def create(self, item: ModelType) -> ModelType:
        item = await self.session.add(item)
        await self.session.commit()
        return item

    async def get_by_attr(self, attr_name: str, value: Any) -> ModelType:
        filter_attr = getattr(self.model, attr_name)
        async with self.session as sess:
            result = await sess.execute(select(self.model).where(filter_attr == value))
            return result.first()

    async def get_by_id(self, id: int) -> ModelType:
        return self.session.query(self.model).filter_by(id=id).first()


