from app.models.user import UserModel

from app.common.database import get_session
from fastapi import APIRouter, Depends
from starlette.requests import Request

from app.repositories.user import Repository
from app.schemas.login import AuthData

login_router = APIRouter()


@login_router.post("/login")
async def login(
        _: Request,
        session: Depends(get_session),
        auth_data: AuthData
):
    repo = Repository(session, UserModel)
    user = await repo.get_by_id()
    return {"message": "login"}